package com.example.psaykham.testmolotov;

import com.example.psaykham.testmolotov.models.MagicSquare;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for the {@link MagicSquare}.
 */
public class MagicSquareUnitTest {

    @Test
    public void testMagicSquareSum1() {
        testMagicSquareSum(0, 3, 15);
    }

    @Test
    public void testMagicSquareSum2() {
        testMagicSquareSum(0, 5, 65);
    }

    @Test
    public void testMagicSquareSum3() {
        testMagicSquareSum(4, 5, 65);
    }

    @Test
    public void testMagicSquareNotEmpty() {
        MagicSquare magicSquare = new MagicSquare(0, 3);
        int[] magicSquareTable = magicSquare.getMagicSquareTable();
        int sum = 0;

        for (int i = 0; i < magicSquareTable.length; i++) {
            Assert.assertNotEquals(0, magicSquareTable[i]);
        }
    }

    private void testMagicSquareSum(int startingPosition, int width, int expectedSum) {
        MagicSquare magicSquare = new MagicSquare(startingPosition, width);
        int[] magicSquareTable = magicSquare.getMagicSquareTable();
        int sum = 0;

        // check "horizontally"
        for (int i = 0; i < magicSquareTable.length; i++) {
            sum += magicSquareTable[i];
            // check i + 1 % 3 instead of i % 3, or else the last one will not be checked
            if (i > 0 && ((i + 1) % width == 0)) {
                // check that the sum is equal to
                Assert.assertEquals(expectedSum, sum);
                sum = 0;
            }
        }

        // check "vertically"
        for (int i = 0; i < magicSquareTable.length / width - 1; i++) {
            int index = i;
            while (index < magicSquareTable.length) {
                sum += magicSquareTable[index];
                index += width;
            }

            Assert.assertEquals(expectedSum, sum);
            sum = 0;
        }
    }
}