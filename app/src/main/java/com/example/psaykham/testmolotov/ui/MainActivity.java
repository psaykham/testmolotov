package com.example.psaykham.testmolotov.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.psaykham.testmolotov.R;
import com.example.psaykham.testmolotov.models.MagicSquare;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    private static final int STARTING_POSITION = 0;
    private static final int SQUARE_WIDTH = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MagicSquare magicSquare = new MagicSquare(STARTING_POSITION, SQUARE_WIDTH);
        Log.d(TAG, magicSquare.toString());
    }
}
