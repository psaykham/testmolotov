package com.example.psaykham.testmolotov.models;

/**
 * Describes a magic square object.
 */
public class MagicSquare {
    private int mWidth;
    private int[] mMagicSquareTable;

    public MagicSquare(int firstPosition, int width) {
        mWidth = width;
        mMagicSquareTable = buildMagicSquare(firstPosition, width);
    }

    /**
     * Builds the magic square for the given initial position and the given square width.
     * @param firstPosition
     * @param width
     * @return
     */
    private int[] buildMagicSquare(int firstPosition, int width) {
        int[] result = new int[width * width];
        int currentPosition = firstPosition;

        result[firstPosition] = 1;
        for (int i = 2; i <= result.length; i++) {
            int newPosition = getNextExpectedPosition(currentPosition, width);
            if (isPositionFilled(newPosition, result)) {
                newPosition = getNextFallbackPosition(currentPosition, width);
            }

            result[newPosition] = i;
            currentPosition = newPosition;
        }

        return result;
    }

    /**
     * @param position
     * @param magicSquare
     * @return true if the given position is already filled inside the magic square, false otherwise.
     */
    private boolean isPositionFilled(int position, int[] magicSquare) {
        if (magicSquare[position] != 0) {
            return true;
        }

        return false;
    }

    /**
     * @param currentPosition
     * @param width
     * @return the next expected position.
     */
    private int getNextExpectedPosition(int currentPosition, int width) {
        int square = width * width;
        int newPosition = currentPosition + 1;
        if (newPosition % width == 0) {
            newPosition = newPosition - width;
        }

        newPosition = newPosition + width;
        if (newPosition >= square) {
            newPosition = newPosition % width;
        }

        return newPosition;
    }

    /**
     * @param currentPosition
     * @param width
     * @return the fallback position.
     */
    private int getNextFallbackPosition(int currentPosition, int width) {
        int newPosition = currentPosition - width;
        if (newPosition < 0) {
            newPosition = currentPosition + (width * (width - 1));
        }

        return newPosition;
    }

    public int[] getMagicSquareTable() {
        return mMagicSquareTable;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < mMagicSquareTable.length; i++) {
            if (i > 0 && i % mWidth == 0) {
                result += "|\n";
            }

            result += "|" + mMagicSquareTable[i];
        }

        result += "|";

        return result;
    }
}
